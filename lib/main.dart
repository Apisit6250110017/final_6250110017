import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp( const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Final Test : Firebase Sample CRUD',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final TextEditingController _firstnameController = TextEditingController();
  final TextEditingController _lastnameController = TextEditingController();
  final TextEditingController _majorController = TextEditingController();
  final TextEditingController _studentController = TextEditingController();

  final CollectionReference _students =
  FirebaseFirestore.instance.collection('students');

  Future<void> _createOrUpdate([DocumentSnapshot? documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      _firstnameController.text = documentSnapshot['firstname'];
      _lastnameController.text = documentSnapshot['lastname'];
      _majorController.text = documentSnapshot['major'];
      _studentController.text = documentSnapshot['students_id'];
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                  controller: _studentController,
                  decoration: const InputDecoration(
                    labelText: 'Studen:ID',
                  ),
                ),
                TextField(
                  controller: _firstnameController,
                  decoration: const InputDecoration(labelText: 'Firstname'),
                ),
                TextField(
                  controller: _lastnameController,
                  decoration: const InputDecoration(labelText: 'lastName'),
                ),
                TextField(
                  controller: _majorController,
                  decoration: const InputDecoration(labelText: 'major'),
                ),

                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String firstname = _firstnameController.text;
                    final String lastname = _lastnameController.text;
                    final String major = _majorController.text;
                    final double? students_id = double.tryParse(_studentController.text);
                    if (firstname != null && students_id!= null) {
                      if (action == 'create') {
                        await _students
                            .add({"firstname": firstname,"lastname": lastname,"major": major, "students_id": students_id})
                            .then((value) => print("students Added"))
                            .catchError((error) =>
                            print("Failed to add students: $error"));
                      }

                      if (action == 'update') {
                        await _students
                            .doc(documentSnapshot!.id)
                            .update({"firstname": firstname,"lastname": lastname,"major": major, "students_id": students_id})
                            .then((value) => print(" student Updated"))
                            .catchError((error) =>
                            print("Failed to update student : $error"));
                      }

                      _firstnameController.text = '';
                      _lastnameController.text = '';
                      _majorController.text = '';
                      _studentController.text = '';


                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  Future<void> _deleteStudents(String studentsId) async {
    await _students
        .doc(studentsId)
        .delete()
        .then((value) => print("students Deleted"))
        .catchError((error) => print("Failed to delete product: $error"));

    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('You have successfully deleted a students')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Final Test:Firebase Sample CRUD'),

      ),
      body: StreamBuilder(
        stream: _students.snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
          if (streamSnapshot.hasData) {
            return ListView.builder(
              itemCount: streamSnapshot.data!.docs.length,
              itemBuilder: (context, index) {
                final DocumentSnapshot documentSnapshot =
                streamSnapshot.data!.docs[index];
                return Card(
                  margin: const EdgeInsets.all(10),
                  child: ListTile(
                    title: Text(documentSnapshot['students_id'].toString()),
                    subtitle: Text(documentSnapshot['firstname']),
                    trailing: SizedBox(
                      width: 100,
                      child: Row(
                        children: [
                          IconButton(
                              icon: const Icon(Icons.edit),
                              onPressed: () =>
                                  _createOrUpdate(documentSnapshot)),
                          IconButton(
                              icon: const Icon(Icons.delete),
                              onPressed: () =>
                                  _deleteStudents(documentSnapshot.id)),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _createOrUpdate(),
        child: const Icon(Icons.add),
      ),
    );
  }
}
